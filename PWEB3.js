document.getElementById('biodataForm').addEventListener('submit', function(e) {
    e.preventDefault();

    const nama = document.getElementById('nama').value;
    const tanggalLahir = document.getElementById('tanggalLahir').value;
    const alamat = document.getElementById('alamat').value;
    const email = document.getElementById('email').value;
    const telepon = document.getElementById('telepon').value;

    const hasil = `
        <h2>Hasil Biodata</h2>
        <p><strong>Nama:</strong> ${nama}</p>
        <p><strong>Tanggal Lahir:</strong> ${tanggalLahir}</p>
        <p><strong>Alamat:</strong> ${alamat}</p>
        <p><strong>Email:</strong> ${email}</p>
        <p><strong>Telepon:</strong> ${telepon}</p>
    `;

    document.getElementById('hasil').innerHTML = hasil;
});
